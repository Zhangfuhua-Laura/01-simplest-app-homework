package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PlusAndMultiplyController {

    public String getPlusOrMultiplyStr(String op){
        String str = "";
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= i; j++) {
                int sum;

                if(op == "+")
                    sum = i + j;
                else
                    sum = i * j;
                str = str + i + op + j + "=" + sum;

                if (sum > 9)
                    str += " ";
                else
                    str += "  ";
            }
            str += "\n";
        }
        return str;
    }

    @GetMapping("/api/tables/plus")
    public String getPlusForm() {
        return getPlusOrMultiplyStr("+");
    }

    @GetMapping("/api/tables/multiply")
    public String getMultiplyForm() {
        System.out.println(getPlusOrMultiplyStr("*"));
        return getPlusOrMultiplyStr("*");
    }
}
